from django.shortcuts import render, redirect
from django.http import HttpResponse
from Douve import getusers

def Register(request):
    """
    """
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    userlist = []
    for u in users:
        userlist.append(u.lower())
    r1 = ""
    r2 = ""
    r3 = ""
    args = {}
    if request.method == 'POST':
        if request.POST['user'] != "" and request.POST['password'] != "" and request.POST['email'] != "":
            if not request.POST['user'].lower() in userlist:
                getusers.addListas(request.POST['user'].capitalize(), request.POST['password'], request.POST['email'])
                picname = "{'Picture':'c.jpg','user':'"+request.POST['user'].capitalize()+"'}"
                getusers.addPicture(picname, request.POST['user'].capitalize())
                getusers.EnviarMsg(request.POST['user'].capitalize(), '', request.POST['user'].capitalize(), '', '', '', '')
                getusers.AddContactsUser(request.POST['user'].capitalize())
                getusers.AddSolicitudes(request.POST['user'].capitalize())
                getusers.AddAceptados(request.POST['user'].capitalize())
                perfilNuevos = "{'usuario':'"+request.POST['user'].capitalize()+"','numero':'Numero','estado':'Estado'}"
                getusers.addEstadoUser(perfilNuevos, request.POST['user'].capitalize())
                return redirect('/login')
            else:
                r1 = "Usuario ya está asignado a alguien mas"
        else:
            if request.POST['password'] == "":
                r3 = "Debe escribir una Contraseña"
            if request.POST['email'] == "":
                r2 = "Debe escribir un Email"
            if request.POST['user'] == "":
                r1 = "Debe escribir un nombre de Usuario"
    args = {'Msg1' : r1, 'Msg2' : r2, 'Msg3' : r3}
    return render(request, 'Apps/Register.html', args)
