from django.shortcuts import redirect

def direct(request):
    return redirect('login/')
