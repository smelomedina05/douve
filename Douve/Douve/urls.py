from django.conf.urls import url, include
from django.contrib import admin
from Douve import views

urlpatterns = [
    url(r'^$', views.direct),
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include('Login.urls')),
    url(r'^home/', include('Home.urls')),
    url(r'^register/', include('Register.urls')),
]
