import os
from Douve.settings import DatosUsrs
import random as ran
from django.utils import timezone
import requests, json

def usuarioslista():
    user = []
    u = open(os.path.join(DatosUsrs, 'Datos/usuarios.txt'), 'r')
    users = u.readlines()
    u.close()
    for n in users:
        user.append(n[:-1])
    return user

def emailslista():
    email = []
    e = open(os.path.join(DatosUsrs, 'Datos/emails.txt'), 'r')
    emails = e.readlines()
    e.close()
    for n in emails:
        email.append(n[:-1])
    return email

def passeslista():
    passes = []
    p = open(os.path.join(DatosUsrs, 'Datos/passes.txt'), 'r')
    passe = p.readlines()
    p.close()
    for n in passe:
        passes.append(n[:-1])
    return passes

def codeslista():
    codes = []
    c = open(os.path.join(DatosUsrs, 'Datos/codesforp.txt'), 'r')
    code = c.readlines()
    c.close()
    for n in code:
        codes.append(n[:-1])
    return codes

def msgslista(user, contacto):
    listamsgs = obtmsgs(user,contacto)
    newdicts = []
    msgsaved = ""
    for item in listamsgs:
        if item['user'] == 'prop':
            msgsaved = item['msg']
            timemsg = item['hora'][:2] + ':' + item['hora'][2:]
            datemsg = item['fecha']
            locmsg = item['posicion']
            imagemsg = item['imagenmsg']
            newdicts.append({'prop':msgsaved, 'conct':'', 'tmsg':timemsg, 'dmsg': datemsg, 'loc': locmsg, 'immsg': imagemsg})
        if item['user'] == 'conct':
            msgsaved = item['msg']
            timemsg = item['hora'][:2] + ':' + item['hora'][2:]
            datemsg = item['fecha']
            locmsg = item['posicion']
            imagemsg = item['imagenmsg']
            newdicts.append({'prop':'', 'conct':msgsaved, 'tmsg':timemsg, 'dmsg': datemsg, 'loc': locmsg, 'immsg': imagemsg})

    return newdicts

def ObtenerTime():
    timezn = str(timezone.localtime(timezone.now()))[:-16]
    timechoosen = timezn[11:]
    getime = timechoosen[:2]+ timechoosen[3:]
    return getime

def ObtenerFecha():
    timezn = str(timezone.now())[:10]
    return timezn

def ObtenerUbicacion():
    ODatos = requests.get('http://freegeoip.net/json')
    TraD = json.loads(ODatos.text)
    Ubicacion = TraD['city'] + ' ' +TraD['country_name']
    return Ubicacion


def obtmsgs(user,contacto):
    Msgs = []
    msg = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+user+'-'+contacto+'.txt'), 'r')
    msgg = msg.readlines()
    msg.close()
    for n in msgg:
        lista = []
        n[:-1]
        for linea in n:
            a = n[1:]
            a = a[:-1]
            b = a.split(',')
            for m in b:
                c = m.split(':')
                for l in c:
                    d = l[1:]
                    d = d[:-1]
                    lista.append(d)
        Msgs.append({lista[0]:lista[1],lista[2]:lista[3],lista[4]:lista[5],lista[6]:lista[7],lista[8]:lista[9],lista[10]:lista[11][:-1]})
    return Msgs

def ProfilePictslista():
    Ppics = []
    pict = open(os.path.join(DatosUsrs, 'Datos/archivos.txt'), 'r')
    ppict = pict.readlines()
    pict.close()
    for n in ppict:
        lista = []
        n[:-1]
        for linea in n:
            a = n[1:]
            a = a[:-1]
            b = a.split(',')
            for m in b:
                c = m.split(':')
                for l in c:
                    d = l[1:]
                    d = d[:-1]
                    lista.append(d)
        Ppics.append({lista[0]:lista[1],lista[2]:lista[3][:-1]})
    return Ppics

def IndexPic(user):
    Count = 0
    pic = ProfilePictslista()
    for p in pic:
        if p['user'] == user:
            return Count
        else:
            Count += 1

def EnviarMsg(user, msgsent, contact, time, date, locate, image):
    if not contact == user:
        msg = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+user+'-'+contact+'.txt'), 'a')
        msg.write("{'user':'prop','msg':'"+msgsent+"','hora':'"+time+"','fecha':'"+date+"','posicion':'"+locate+"','imagenmsg':'"+image+"'}"+"\n")
        msg.close()
        msgconct = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+contact+'-'+user+'.txt'), 'a')
        msgconct.write("{'user':'conct','msg':'"+msgsent+"','hora':'"+time+"','fecha':'"+date+"','posicion':'"+locate+"','imagenmsg':'"+image+"'}"+"\n")
        msgconct.close()
    else:
        msg = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+user+'-'+contact+'.txt'), 'a')
        msg.write("{'user':'prop','msg':'"+msgsent+"','hora':'"+time+"','fecha':'"+date+"','posicion':'"+locate+"','imagenmsg':'"+image+"'}"+"\n")
        msg.close()

    return "Enviado!"

def DelActivosE(userE):
    activos = ''
    estados = ObtenerEstado()
    if userE in estados:
        estados.remove(userE)
    for e in estados:
        activos = activos + e + '\n'
    est = open(os.path.join(DatosUsrs, 'Datos/activos.txt'), 'w')
    est.write(activos)
    est.close()
    return 'Inactivo!'

def ObtenerEstado():
    estados = []
    est = open(os.path.join(DatosUsrs, 'Datos/activos.txt'), 'r')
    estad = est.readlines()
    est.close()
    for e in estad:
        estados.append(e[:-1])
    return estados

def AgregarActivosE(userE):
    estados = ObtenerEstado()
    if not userE in estados:
        est = open(os.path.join(DatosUsrs, 'Datos/activos.txt'), 'a')
        est.write(userE+'\n')
        est.close()
    return 'Activo!'


def GetImagenesNombres():
    imgs = []
    ims = open(os.path.join(DatosUsrs, 'Datos/Imagenes.txt'), 'r')
    imas = ims.readlines()
    for i in imas:
        imgs.append(i[:-1])
    return imgs


def ObtenerNameConver(usuario):
    contactos = open(os.path.join(DatosUsrs, 'Datos/EnConversacion/'+usuario+'.txt'), 'r')
    contacts = contactos.readlines()
    contactos.close()
    listacntcs = []
    for c in contacts:
        listacntcs.append(c[:-1])
    return listacntcs

def ObtenerNameContact(usuario):
    contactos = open(os.path.join(DatosUsrs, 'Datos/ListasContactos/'+usuario+'.txt'), 'r')
    contacts = contactos.readlines()
    contactos.close()
    listacntcs = []
    for c in contacts:
        listacntcs.append(c[:-1])
    return listacntcs

def ObtenerPictsCtcs(Contcs):
    listaCtcsPicts = []
    for con in Contcs:
        pPict = ProfilePictslista()
        Saved = pPict[IndexPic(con)]['Picture']
        listaCtcsPicts.append(Saved)

    return listaCtcsPicts


def ObtenerMsgContact(usuario, contacto):
    msgelegidos = obtmsgs(usuario, contacto)
    msgelegido = ""
    if len(msgelegidos) != 0:
        msgelegido = msgelegidos[-1]['msg']
    return msgelegido

def ObtenerHoraMsgContact(usuario, contacto):
    horaelegidos = obtmsgs(usuario, contacto)
    horaelegido = ""
    if len(horaelegidos) != 0:
        horaelegido = horaelegidos[-1]['hora']
        horaelegido = horaelegido[:2] + ":" + horaelegido[2:]
    return horaelegido


def ObtenerDictCover(user, cntc):
    lc = ObtenerNameConver(user)
    ln = ObtenerPictsCtcs(lc)
    ec = ObtenerEstado()
    indice = 0
    listadicts = []
    for n in lc:
        if n in ec:
            estados = 'En Linea'
            etip = 'contactconverec'
        else:
            estados = 'Fuera'
            etip = 'contactconverec2'
        if not n == cntc:
            msgelegido = ObtenerMsgContact(user, n)
            horaelegido = ObtenerHoraMsgContact(user, n)
            dictm = {'contacto': n, 'image': 'UsersPicts/'+ln[indice], 'msg': msgelegido, 'hora': horaelegido, 'chatActivo': 'white', 'state': estados, 'estadoT': etip}
            indice += 1
            listadicts.append(dictm)
        else:
            msgelegido = ObtenerMsgContact(user, n)
            horaelegido = ObtenerHoraMsgContact(user, n)
            dictm = {'contacto': n, 'image': 'UsersPicts/'+ln[indice], 'msg': msgelegido, 'hora': horaelegido, 'chatActivo': '#f3f3f3', 'state': estados, 'estadoT': etip}
            indice += 1
            listadicts.append(dictm)
    return listadicts


def ObtenerDictCtcs(user):
    lc = ObtenerNameContact(user)
    ln = ObtenerPictsCtcs(lc)
    indice = 0
    listadicts = []
    for n in lc:
        dato = '/home/entry/ACon/' + user + '/' + n + '/'
        msgelegido = ObtenerMsgContact(user, n)
        horaelegido = ObtenerHoraMsgContact(user, n)
        estadoelegido = GetEstadoUser(n)['estado']
        numeroelegido = GetEstadoUser(n)['numero']
        dictm = {'contacto': n, 'image': 'UsersPicts/'+ln[indice], 'msg': msgelegido, 'hora': horaelegido, 'contactoMsg': dato, 'estado': estadoelegido, 'numero': numeroelegido}
        indice += 1
        listadicts.append(dictm)
    return listadicts


def ObtenerSolicitudConct(usuario):
    sol = GetSolicitudes(usuario)
    ln = ObtenerPictsCtcs(sol)
    indice = 0
    solicitudeslista = []
    for s in GetSolicitudes(usuario):
        solicitudeslista.append({'solicitud': s, 'imagen': 'UsersPicts/'+ln[indice]})
        indice += 1
    return solicitudeslista


def ObtenerDictCtcs2(user, letras):
    lc = ObtenerNameContact(user)
    ln = ObtenerPictsCtcs(lc)
    indice = 0
    listadicts = []
    for n in lc:
        saved = ""
        estadoelegido = GetEstadoUser(n)['estado']
        numeroelegido = GetEstadoUser(n)['numero']
        for l in n:
            saved = saved + l
            if saved == letras:
                dictm = {'contacto': n, 'image': 'UsersPicts/'+ln[indice], 'estado': estadoelegido, 'numero': numeroelegido}
                listadicts.append(dictm)
        indice += 1
    return listadicts


def ObtenerDictCtcs3(user, letras):
    lc = usuarioslista()
    lac = ObtenerNameContact(user)
    ln = ObtenerPictsCtcs(lc)
    indice = 0
    listadicts = []
    for n in lc:
        solicitudes = GetSolicitudes(n)
        estadoelegido = GetEstadoUser(n)['estado']
        numeroelegido = GetEstadoUser(n)['numero']
        if user in solicitudes:
            msgtp = "avisoSolicitud('Solicitud a " + n + " ya fue enviada anteriormente');"
        else:
            msgtp = "avisoSolicitud('Solicitud de amistad enviada a " + n + "');"

        dato = '/home/entry/'+user+'/Contactos/' + n + '/'
        if n != user and not n in lac:
            saved = ""
            for l in n:
                saved = saved + l
                if saved == letras:
                    dictm = {'contacto': n, 'image': 'UsersPicts/'+ln[indice], 'contactoMsg': dato, 'tipomensaje': msgtp, 'estado': estadoelegido, 'numero': numeroelegido}
                    listadicts.append(dictm)
        indice += 1
    return listadicts


def AcceptedFile(File):
    cuenta = 0
    fileext = File
    for n in File:
        if n == ".":
            cuenta += 1
    if cuenta == 1:
        saved = ""
        getim = False
        for l in File:
            if getim:
                saved = saved + l
            if l == ".":
                getim = True
        return saved
    else:
        for p in range(cuenta-1):
            posicion = 0
            for c in fileext:
                if c == ".":
                    f = list(fileext)
                    f[posicion] = "_"
                    fileext = "".join(f)
                    break
                posicion += 1
        saved = ""
        getim = False
        for l in fileext:
            if getim:
                saved = saved + l
            if l == ".":
                getim = True
        return saved

def ObtElementosEstadoNumero():
    Est = []
    esta = open(os.path.join(DatosUsrs, 'Datos/estados.txt'), 'r')
    estas = esta.readlines()
    esta.close()
    for n in estas:
        lista = []
        n[:-1]
        for linea in n:
            a = n[1:]
            a = a[:-1]
            b = a.split(',')
            for m in b:
                c = m.split(':')
                for l in c:
                    d = l[1:]
                    d = d[:-1]
                    lista.append(d)
        Est.append({lista[0]:lista[1],lista[2]:lista[3],lista[4]:lista[5][:-1]})
    return Est

def GetEstadoUser(user):
    Estados = ObtElementosEstadoNumero()
    edict = {}
    for u in Estados:
        if u['usuario'] == user:
            edict = u
            break
    return edict


def addEstadoUser(info, usuario):
    est = ObtElementosEstadoNumero()
    clist = []
    conteo = 0
    finaldef = ""
    for item in est:
        if item['usuario'] == usuario:
            clist.append(int(est.index(item)))
    for num in clist:
        del(est[num - conteo])
        conteo += 1
    esta = open(os.path.join(DatosUsrs, 'Datos/estados.txt'), 'w')
    for elemento in est:
        nuevo = "{'usuario':'"+elemento['usuario']+"','numero':'"+elemento['numero']+"','estado':'"+elemento['estado']+"'}"
        finaldef = finaldef + nuevo + "\n"
    esta.write(finaldef)
    esta.close()
    esta = open(os.path.join(DatosUsrs, 'Datos/estados.txt'), 'a')
    esta.write(info+'\n')
    esta.close()
    return "Agregado!"



def GetSolicitudes(user):
    solic = []
    so = open(os.path.join(DatosUsrs, 'Datos/Solicitudes/' + user + '.txt'), 'r')
    sol = so.readlines()
    for s in sol:
        solic.append(s[:-1])
    return solic

def GetAceptados(user):
    acep = []
    ac = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + user + '.txt'), 'r')
    ace = ac.readlines()
    for a in ace:
        acep.append(a[:-1])
    return acep


def DelSolicitudes(user, nombre):
    acep = []
    ac = open(os.path.join(DatosUsrs, 'Datos/Solicitudes/' + user + '.txt'), 'r')
    ace = ac.readlines()
    for a in ace:
        acep.append(a)
    acep.remove(nombre+'\n')
    naceptar = ""
    for at in acep:
        naceptar = naceptar + at
    ac = open(os.path.join(DatosUsrs, 'Datos/Solicitudes/' + user + '.txt'), 'w')
    ac.write(naceptar)
    return 'Aceptado!'

def DelAceptados(user, nombre):
    acep = []
    ac = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + user + '.txt'), 'r')
    ace = ac.readlines()
    for a in ace:
        acep.append(a)
    acep.remove(nombre+'\n')
    naceptar = ""
    for at in acep:
        naceptar = naceptar + at
    ac = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + user + '.txt'), 'w')
    ac.write(naceptar)
    return 'Agregado!'


def AddNewSolicitudes(user, nombre):
    if not user in GetSolicitudes(nombre) and not nombre in GetSolicitudes(user):
        s = open(os.path.join(DatosUsrs, 'Datos/Solicitudes/' + nombre + '.txt'), 'a')
        s.write(str(user)+'\n')
        s.close()


def AddNewAceptados(user, nombre):
    if nombre in GetSolicitudes(user):
        DelSolicitudes(user, nombre)
        s = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + user + '.txt'), 'a')
        s.write(str(nombre)+'\n')
        s.close()
        sd = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + nombre + '.txt'), 'a')
        sd.write(str(user)+'\n')
        sd.close()


def AddAceptados(user):
    s = open(os.path.join(DatosUsrs, 'Datos/Aceptados/' + user + '.txt'), 'w')
    s.write('')
    s.close()

def AddSolicitudes(user):
    s = open(os.path.join(DatosUsrs, 'Datos/Solicitudes/' + user + '.txt'), 'w')
    s.write('')
    s.close()

def AddContactsUser(usuario):
    contactos = open(os.path.join(DatosUsrs, 'Datos/ListasContactos/'+usuario+'.txt'), 'w')
    contactos.write('')
    contactos.close()
    contactosC = open(os.path.join(DatosUsrs, 'Datos/EnConversacion/'+usuario+'.txt'), 'w')
    contactosC.write('')
    contactosC.close()

def AddContactsUserConver(usuario, conct):
    Cconf = []
    contactosR = open(os.path.join(DatosUsrs, 'Datos/EnConversacion/'+usuario+'.txt'), 'r')
    cR = contactosR.readlines()
    contactosR.close()
    for c in cR:
        Cconf.append(c[:-1])
    if not conct in Cconf:
        contactosC = open(os.path.join(DatosUsrs, 'Datos/EnConversacion/'+usuario+'.txt'), 'a')
        contactosC.write(conct + '\n')
        contactosC.close()

def AddContactsUserCtcs(usuario, conct):
    Cconf = []
    contactosR = open(os.path.join(DatosUsrs, 'Datos/ListasContactos/'+usuario+'.txt'), 'r')
    cR = contactosR.readlines()
    contactosR.close()
    for c in cR:
        Cconf.append(c[:-1])
    if not conct in Cconf:
        lusers = usuarioslista()
        if conct in lusers:
            if not usuario in GetSolicitudes(conct) and not conct in GetAceptados(usuario):
                AddNewSolicitudes(usuario, conct)


def AgregarContactosAceptar(usuario):
    Contactos = GetAceptados(usuario)
    for c in Contactos:
        if not c in ObtenerNameContact(usuario):
            DelAceptados(usuario, c)
            contactos = open(os.path.join(DatosUsrs, 'Datos/ListasContactos/'+usuario+'.txt'), 'a')
            contactos.write(c + '\n')
            contactos.close()
            contactosCnsU = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+usuario+'-'+c+'.txt'), 'w')
            contactosCnsU.write('')
            contactosCnsU.close()
            contactosCnsC = open(os.path.join(DatosUsrs, 'Datos/MsgsUsers/'+c+'-'+usuario+'.txt'), 'w')
            contactosCnsC.write('')
            contactosCnsC.close()
            DelAceptados(c, usuario)
            contactos = open(os.path.join(DatosUsrs, 'Datos/ListasContactos/'+c+'.txt'), 'a')
            contactos.write(usuario + '\n')
            contactos.close()


def addListas(l1, l2, l3):
    u = open(os.path.join(DatosUsrs, 'Datos/usuarios.txt'), 'a')
    u.write(str(l1+'\n'))
    u.close()
    e = open(os.path.join(DatosUsrs, 'Datos/emails.txt'), 'a')
    e.write(str(l3+'\n'))
    e.close()
    p = open(os.path.join(DatosUsrs, 'Datos/passes.txt'), 'a')
    r1 = ran.randint(0,100)
    r2 = ran.randint(0,100)
    r3 = ran.randint(0,100)
    p.write("{'passsaved':'" + str(l2) + "' , 'codefp':'" + (str(r1)+str(r2)+str(r3)) + "'}" + "\n")
    p.close()
    c = open(os.path.join(DatosUsrs, 'Datos/codesforp.txt'), 'a')
    c.write((str(r1)+str(r2)+str(r3))+"\n")
    c.close()
    return "Agregado!"

def addPictFile(user, sfile, tp, pcname):
    if tp == 'cuenta':
        pict = open(os.path.join(DatosUsrs, 'Home/static/UsersPicts/'+user+'.jpg'), 'wb')
        pictf = sfile.readlines()
        save = b''
        for p in pictf:
            save = save + p
        pict.write(save)
    else:
        if tp == 'msg':
            pict = open(os.path.join(DatosUsrs, 'Home/static/PicsMsg/'+pcname), 'wb')
            pictf = sfile.readlines()
            save = b''
            for p in pictf:
                save = save + p
            pict.write(save)
            addNamePicMsg(pcname)


def addNamePicMsg(img):
    s = open(os.path.join(DatosUsrs, 'Datos/Imagenes.txt'), 'a')
    s.write(str(img)+'\n')
    s.close()


def addPicture(info, usuario):
    pics = ProfilePictslista()
    clist = []
    conteo = 0
    finaldef = ""
    for item in pics:
        if item['user'] == usuario:
            clist.append(int(pics.index(item)))
    for num in clist:
        del(pics[num - conteo])
        conteo += 1
    pictd = open(os.path.join(DatosUsrs, 'Datos/archivos.txt'), 'w')
    for elemento in pics:
        nuevo = "{'Picture':'"+elemento['Picture']+"','user':'"+elemento['user']+"'}"
        finaldef = finaldef + nuevo + "\n"
    pictd.write(finaldef)
    pictd.close()
    pictd = open(os.path.join(DatosUsrs, 'Datos/archivos.txt'), 'a')
    pictd.write(info+'\n')
    pictd.close()
    return "Agregado!"

def sesslista():
    sess = []
    s = open(os.path.join(DatosUsrs, 'Datos/sesiones.txt'), 'r')
    sessi = s.readlines()
    s.close()
    for n in sessi:
        sess.append(n[:-1])
    return sess

def addSess(info):
    sessi = open(os.path.join(DatosUsrs, 'Datos/sesiones.txt'), 'a')
    sessi.write(info+'\n')
    sessi.close()
    return "Agregado!"

def delSess(info):
    s = open(os.path.join(DatosUsrs, 'Datos/sesiones.txt'), 'r')
    sessi = s.readlines()
    s.close()
    sessi.remove(info+'\n')
    news = ""
    for sn in sessi:
        news = news + sn
    sessis = open(os.path.join(DatosUsrs, 'Datos/sesiones.txt'), 'w')
    sessis.write(news)
    sessis.close()
    return "Borrado!"
