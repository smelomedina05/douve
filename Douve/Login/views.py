from django.shortcuts import render, redirect
from django.http import HttpResponse
from Douve import getusers
import random as ran

def LogIn(request):
    """
    """
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    codes = getusers.codeslista()
    args = {}
    userlist = []
    for u in users:
        userlist.append(u.lower())
    r1 = ""
    r2 = ""
    if 'user' in request.COOKIES.keys():
        return redirect('/home/entry/Cuenta/'+request.COOKIES.get('user')+'/0000')
    else:
        if request.method == 'POST':
            texto = str(request.POST['user'].capitalize())
            password = str(request.POST['password'])
            if texto != "" and password != "":
                if texto.lower() in userlist:
                    indexu = users.index(texto)
                    codefp = codes[indexu]
                    searchp = "{'passsaved':'"+password+"' , 'codefp':'"+codefp+"'}"
                    if searchp in passes:
                        rn = ran.randint(0,1000)
                        rn1 = ran.randint(0,1000)
                        rn2 = ran.randint(0,1000)
                        sess = str(rn) + str(rn1) + str(rn2)
                        getusers.addSess(sess)
                        getusers.AgregarActivosE(texto)
                        return redirect('/home/entry/Cuenta/'+texto+'/'+sess)
                        #return redirect('/miu/home/'+texto+'/'+sess)
                    else:
                        r2 = "Contraseña Incorrecta"
                else:
                        r1 = "Usuario No existe"
            else:
                if texto == "":
                    r1 = "Debe escribir un nombre de Usuario"
                if password == "":
                    r2 = "Debe escribir una Contraseña"

        args = {'Msg1': r1, 'Msg2': r2}
        return render(request, "Apps/Login.html", args)
