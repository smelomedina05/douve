from django.shortcuts import render, redirect
from django.http import HttpResponse
from Douve import getusers
from django.utils import timezone
import random as ran


def Homepage(request):
    if 'user' in request.COOKIES.keys():
        getusers.DelActivosE(request.COOKIES.get('user'))
        renderizar = redirect('/login/')
        renderizar.delete_cookie('user')
        renderizar.delete_cookie('pass')
        return renderizar
    else:
        return redirect('/login/')


def AgContactos(request, usernamen, conct):
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        getusers.AddContactsUserCtcs(usernamen, conct)
        return redirect('/home/entry/'+usernamen+'/Contactos/')
    else:
        return redirect('/home/entry/'+usernamen+'/Contactos/')

def AcceptContactos(request, usernamen, conct):
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        getusers.AddNewAceptados(usernamen, conct)
        return redirect('/home/entry/'+usernamen+'/Contactos/')
    else:
        return redirect('/home/entry/'+usernamen+'/Contactos/')


def DenyContactos(request, usernamen, conct):
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        getusers.DelSolicitudes(usernamen, conct)
        return redirect('/home/entry/'+usernamen+'/Contactos/')
    else:
        return redirect('/home/entry/'+usernamen+'/Contactos/')


def EnContactos(request, usernamen, conct):
    users = getusers.usuarioslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        getusers.AddContactsUserConver(usernamen, conct)
        return redirect('/home/entry/'+usernamen+'/Conversaciones/'+conct)
    else:
        return redirect('/home/entry/'+usernamen+'/Contactos/')

def Cuenta(request, usernamen, num):
    users = getusers.usuarioslista()
    emails = getusers.emailslista()
    sess = getusers.sesslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    pPict = getusers.ProfilePictslista()
    Saved = pPict[getusers.IndexPic(usernamen)]['Picture']
    NumContacto = getusers.GetEstadoUser(usernamen)['numero']
    EstContacto = getusers.GetEstadoUser(usernamen)['estado']
    ifile = ""
    if num in getusers.sesslista():
        getusers.delSess(num)
        args = {'NUsuario': usernamen ,
        'NEmail': emails[index],
        'ColorCuenta': '#33b6ea',
        'ColorContactos': 'white',
        'ColorConversaciones': 'white',
        'ColorNosotros': 'white',
        'ProfilePict': 'UsersPicts/' + Saved,
        'Numero': NumContacto,
        'Estado': EstContacto,
        }

        renderizar = render(request, 'Apps/Cuenta.html', args)
        renderizar.set_cookie('user', usernamen)
        renderizar.set_cookie('pass', passes[index])
        return renderizar

    else:
        if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
            args = {'NUsuario': usernamen ,
            'NEmail': emails[index],
            'ColorCuenta': '#33b6ea',
            'ColorContactos': 'white',
            'ColorConversaciones': 'white',
            'ColorNosotros': 'white',
            'ProfilePict': 'UsersPicts/' + Saved,
            'Numero': NumContacto,
            'Estado': EstContacto,
            }

            renderizar = render(request, 'Apps/Cuenta.html', args)
            if request.method == "POST":
                datapicture = request.FILES.get('Pic_File')
                if datapicture:
                    extension = getusers.AcceptedFile(request.FILES['Pic_File'].name)
                    if extension == "jpg" or extension == "gif" or extension == "png" or extension == "JPG" or extension == "GIF" or extension == "PNG" or extension == "JPGE":
                        getusers.addPictFile(usernamen, request.FILES['Pic_File'], 'cuenta', '')
                        picname = "{'Picture':'"+usernamen+'.jpg'+"','user':'"+usernamen+"'}"
                        getusers.addPicture(picname, usernamen)
                        return redirect('/home/entry/Cuenta/'+usernamen+'/0000/')
                    else:
                        args = {'NUsuario': usernamen ,
                        'NEmail': emails[index],
                        'ColorCuenta': '#33b6ea',
                        'ColorContactos': 'white',
                        'ColorConversaciones': 'white',
                        'ColorNosotros': 'white',
                        'ProfilePict': 'UsersPicts/' + Saved,
                        'ArchivoAlerta': 'AlertaImagen(',
                        'mensajealerta': 'El archivo a enviar no es valido',
                        'ArchivoAlerta2': ');',
                        'Numero': NumContacto,
                        'Estado': EstContacto,
                        }
                        renderizar = render(request, 'Apps/Cuenta.html', args)
                else:
                    if request.POST['numero'] != "":
                        NumContacto = str(request.POST['numero'])
                    if request.POST['estado'] != "":
                        EstContacto = str(request.POST['estado'])
                    if NumContacto != "" or EstContacto != "":
                        perfilNuevos = "{'usuario':'"+usernamen+"','numero':'"+NumContacto+"','estado':'"+EstContacto+"'}"
                        getusers.addEstadoUser(perfilNuevos, usernamen)
                        return redirect('/home/entry/Cuenta/'+usernamen+'/0000/')
            return renderizar
        else:
            return redirect('/login/')

def Contactos(request, usernamen):
    users = getusers.usuarioslista()
    sess = getusers.sesslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    getusers.AgregarContactosAceptar(usernamen)
    LConctactsConver = getusers.ObtenerDictCtcs(usernamen)
    PlaceHolderAg = ""
    Solicitar = False
    solicitudeslista = getusers.ObtenerSolicitudConct(usernamen)
    CSolic = ""
    CanSolic = 0
    if len(solicitudeslista) > 0:
        CSolic = "#bcbcbc"
        CanSolic = len(solicitudeslista)
    else:
        CSolic = "transparent"
        CanSolic = 0
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        if request.method == 'POST':
            Buscar = request.POST.get('Busqueda')
            Agregar = request.POST.get('sumarsubmit')
            Solicitar = request.POST.get('Solicitud')
            if Buscar:
                ContBusqueda = request.POST['Busqueda']
                LConctactsConver = getusers.ObtenerDictCtcs2(usernamen, ContBusqueda)
            if Agregar:
                PlaceHolderAg = [{'MsgmPH': 'Escriba un nombre'}]
                LConctactsConver = []
            if Solicitar:
                PlaceHolderAg = [{'MsgmPH': 'Escriba un nombre'}]
                Solicito = request.POST['Solicitud']
                LConctactsConver = getusers.ObtenerDictCtcs3(usernamen, Solicito)
                print(LConctactsConver)

        args = {'NUsuario': usernamen ,
        'ColorCuenta': 'white',
        'ColorContactos': '#33b6ea',
        'ColorConversaciones': 'white',
        'ColorNosotros': 'white',
        'DatosContactsConver': LConctactsConver,
        'PlaceHolder': PlaceHolderAg,
        'Solicitudes': solicitudeslista,
        'ColorSol': CSolic,
        'CantidadSolicitudes': CanSolic,
        }
        renderizar = render(request, 'Apps/Contactos.html', args)
        return renderizar
    else:
        return redirect('/login/')

def Conversacion(request, usernamen, contacto):
    users = getusers.usuarioslista()
    sess = getusers.sesslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if len(getusers.msgslista(usernamen,contacto)) > 0:
        Msgs = getusers.msgslista(usernamen,contacto)
    else:
        Msgs = ""
    LConctactsConver = getusers.ObtenerDictCover(usernamen, contacto)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        if request.method == 'POST':
            if request.POST['Msg'] != "":
                getime = getusers.ObtenerTime()
                getdate = getusers.ObtenerFecha()
                getloc = getusers.ObtenerUbicacion()
                getusers.AddContactsUserConver(contacto, usernamen)
                getusers.EnviarMsg(usernamen, request.POST['Msg'], contacto, getime, getdate, getloc, '')
                return redirect('/home/entry/'+usernamen+'/Conversaciones/'+contacto)
            else:
                if request.FILES['filein'] != "":
                    imagenmsg = request.FILES['filein'].name
                    extension = getusers.AcceptedFile(imagenmsg)
                    if extension == "jpg" or extension == "gif" or extension == "png" or extension == "JPG" or extension == "GIF" or extension == "PNG" or extension == "JPGE":
                        if not imagenmsg in getusers.GetImagenesNombres():
                            imagenmsg = imagenmsg
                        else:
                            code0 = ran.randint(0,1000)
                            code1 = ran.randint(0,1000)
                            code2 = ran.randint(0,1000)
                            codes = str(code0)+str(code1)+str(code2)
                            imagenmsg = codes + imagenmsg
                        getusers.addPictFile('', request.FILES['filein'], 'msg', imagenmsg)
                        callimage = "PicsMsg/" + imagenmsg
                        getime = getusers.ObtenerTime()
                        getdate = getusers.ObtenerFecha()
                        getloc = getusers.ObtenerUbicacion()
                        getusers.AddContactsUserConver(contacto, usernamen)
                        getusers.EnviarMsg(usernamen, ' ', contacto, getime, getdate, getloc, callimage)
                        return redirect('/home/entry/'+usernamen+'/Conversaciones/'+contacto)
                    else:
                        args = {'NUsuario': usernamen ,
                                'ColorCuenta': 'white',
                                'ColorContactos': 'white',
                                'ColorConversaciones': '#33b6ea',
                                'ColorNosotros': 'white',
                                'Msgs': Msgs,
                                'Conct': contacto,
                                'DatosContactsConver': LConctactsConver,
                                'ArchivoAlerta': 'AlertaImagen(',
                                'mensajealerta': 'El archivo a enviar no es valido',
                                'ArchivoAlerta2': ');',
                                }
                        renderizar = render(request, 'Apps/Conversaciones.html', args)
                        return renderizar

        args = {'NUsuario': usernamen ,
                'ColorCuenta': 'white',
                'ColorContactos': 'white',
                'ColorConversaciones': '#33b6ea',
                'ColorNosotros': 'white',
                'Msgs': Msgs,
                'Conct': contacto,
                'DatosContactsConver': LConctactsConver,

                }

        renderizar = render(request, 'Apps/Conversaciones.html', args)
        return renderizar
    else:
        return redirect('/login/')

def Nosotros(request, usernamen):
    users = getusers.usuarioslista()
    sess = getusers.sesslista()
    passes = getusers.passeslista()
    index = users.index(usernamen)
    if 'user' in request.COOKIES.keys() and request.COOKIES.get('user') == usernamen and request.COOKIES.get('pass') == passes[index]:
        args = {'NUsuario': usernamen ,
        'ColorCuenta': 'white',
        'ColorContactos': 'white',
        'ColorConversaciones': 'white',
        'ColorNosotros': '#33b6ea'}

        renderizar = render(request, 'Apps/Nosotros.html', args)
        return renderizar
    else:
        return redirect('/login/')
