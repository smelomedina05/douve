from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^entry/Cuenta/(.+)/(\d+)/$', views.Cuenta),
    url(r'^salir/', views.Homepage),
    url(r'^entry/(.+)/Contactos/$', views.Contactos),
    url(r'^entry/(?P<usernamen>.+)/Conversaciones/(?P<contacto>.+)/$', views.Conversacion, name="Conversaciones"),
    url(r'^entry/(.+)/Nosotros/$', views.Nosotros),
    url(r'^entry/(.+)/Contactos/(.+)/$', views.AgContactos),
    url(r'^entry/ACon/(.+)/(.+)/$', views.EnContactos),
    url(r'^entry/AAccpt/(.+)/(.+)/$', views.AcceptContactos),
    url(r'^entry/DDeny/(.+)/(.+)/$', views.DenyContactos),
]
